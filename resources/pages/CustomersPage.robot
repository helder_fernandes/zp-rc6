*** Settings ***
Documentation           Representação da página clientes com seus elementos e ações

*** Variables ***
#Android
${CUSTOMERS_FORM}       css:a[href$=register]
${LABEL_NAME}           css:label[for=name]    
${LABEL_CPF}            css:label[for=cpf]
${LABEL_ADDRESS}        css:label[for=address]
${LABEL_PHONE}          css:label[for=phone_number]

#iOS
${CUSTOMERS_FORM}       css:a[href$=register]
${LABEL_NAME}           css:label[for=name]    
${LABEL_CPF}            css:label[for=cpf]
${LABEL_ADDRESS}        css:label[for=address]
${LABEL_PHONE}          css:label[for=phone_number]
*** Settings ***

Library         SeleniumLibrary

Library         libs/db.py

# Resource        kws.robot
Resource        hooks.robot

Resource        components/Sidebar.robot
Resource        components/Toaster.robot
Resource        pages/CustomersPage.robot
Resource        pages/LoginPage.robot
Resource        keywords/kws_login.robot
Resource        keywords/kws_customer.robot

*** Variables ***
# Simples
${base_url}         http://zepalheta-web:3000/
${admin_user}       admin@zepalheta.com.br
${admin_password}   pwd123
*** Settings ***


## Login

*** Keywords ***
Acesso a página Login
    Go To       ${base_url}

Submeto minhas credenciais
    [Arguments]     ${email}        ${password}

    Login With  ${email}        ${password}

Devo ver a área logada    
    Wait Until Page Contains        Aluguéis     5


Devo ver um toaster com a mensagem
    [Arguments]         ${expect_message}

    Wait Until Element Contains     ${TOASTER_ERROR_LOGIN}         ${expect_message}

Login With
    [Arguments]     ${email}        ${password}

    Input Text      ${INPUT_EMAIL}          ${email}
    Input Text      ${INPUT_PASSWORD}       ${password} 
    Click Element   ${BUTTON_ENTRAR}
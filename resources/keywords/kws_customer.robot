*** Settings ***

## Customers

*** Keywords ***
Register New Customer
    [Arguments]     ${name}     ${cpf}     ${address}     ${phone_number}

    Input Text      id:name                ${name}
    Input Text      id:cpf                 ${cpf}
    Input Text      id:address             ${address}
    Input Text      id:phone_number        ${phone_number}
    
    Click Element   xpath://button[text()='CADASTRAR']

Dado que acesso o formulário de cadastro de clientes
    Wait Until Element Is Visible       ${NAV_CUSTOMERS}        5
    Click Element                       ${NAV_CUSTOMERS}
    Wait Until Element Is Visible       ${CUSTOMERS_FORM}       5
    Click Element                       ${CUSTOMERS_FORM}

E que eu tenha o seguinte cliente:
    [Arguments]             ${name}     ${cpf}     ${address}     ${phone_number}

    Remove Customer By Cpf  ${cpf}

    Set Test Variable       ${name}
    Set Test Variable       ${cpf}
    Set Test Variable       ${address}
    Set Test Variable       ${phone_number}

Mas esse cpf já existe no sistema
    Insert Customer     ${name}     ${cpf}     ${address}     ${phone_number}

Quando faço a inclusão desse cliente
    Register New Customer   ${name}     ${cpf}     ${address}     ${phone_number}

Então devo ver a notificação:
    [Arguments]                         ${expected_notice}

    Wait Until Element Contains         ${TOASTER_SUCCESS}        ${expected_notice}

Então devo ver a notificação de erro:
    [Arguments]                         ${expected_notice}

    Wait Until Element Contains         ${TOASTER_ERROR}        ${expected_notice}        5

Então devo ver mensagens informando que os campos do cadastro de clientes são obrigatórios
    Wait Until Element Contains        ${LABEL_NAME}         Nome é obrigatório          5
    Wait Until Element Contains        ${LABEL_CPF}          CPF é obrigatório           5
    Wait Until Element Contains        ${LABEL_ADDRESS}      Endereço é obrigatório      5
    Wait Until Element Contains        ${LABEL_PHONE}        Telefone é obrigatório      5

Então devo ver a mensagem
    [Arguments]         ${expect_error}

    Wait Until Page Contains    ${expect_error}

Então devo ver o texto:
    [Arguments]     ${expect_text}

    Wait Until Page Contains      ${expect_text}    5  



*** Settings ***
Documentation       Login

Resource        ../resources/base.robot

#Executa uma ou mais Keywords antes da execução de todos os casos de teste
Test Setup          Start Session
# Executa uma ou mais Keywords após cada execução de testes
Test Teardown       Finish Session


*** Test Cases ***
Login do Administrador
    Acesso a página Login
    Submeto minhas credenciais      admin@zepalheta.com.br  pwd123
    Devo ver a área logada
